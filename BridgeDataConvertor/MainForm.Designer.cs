﻿namespace BridgeDataConvertor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.inputFileTxt = new System.Windows.Forms.TextBox();
            this.inputFileLbl = new System.Windows.Forms.Label();
            this.MainTabCtrl = new System.Windows.Forms.TabControl();
            this.MainTab = new System.Windows.Forms.TabPage();
            this.msgRchTxt1 = new System.Windows.Forms.RichTextBox();
            this.outputFolderOpnBtn = new System.Windows.Forms.Button();
            this.outputFolderLbl = new System.Windows.Forms.Label();
            this.outputFolderTxt = new System.Windows.Forms.TextBox();
            this.inputFileOpnBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AllCulvertRB = new System.Windows.Forms.RadioButton();
            this.NonCulvertRB = new System.Windows.Forms.RadioButton();
            this.assumptionGrp = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.assumptionAllRdb = new System.Windows.Forms.RadioButton();
            this.assumptionOnSystemRdb = new System.Windows.Forms.RadioButton();
            this.BridgeFragilityTab = new System.Windows.Forms.TabPage();
            this.SkewConsidirationCKB = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OtherClassesGrpBox = new System.Windows.Forms.GroupBox();
            this.CustomAndHazusRB = new System.Windows.Forms.RadioButton();
            this.AllCustomRB = new System.Windows.Forms.RadioButton();
            this.AllHazusRB = new System.Windows.Forms.RadioButton();
            this.CompleteLbl = new System.Windows.Forms.Label();
            this.msgRchTxt = new System.Windows.Forms.RichTextBox();
            this.ExtensiveLbl = new System.Windows.Forms.Label();
            this.ModerateLbl = new System.Windows.Forms.Label();
            this.SlightLbl = new System.Windows.Forms.Label();
            this.BridgeFragilityGrdViw = new System.Windows.Forms.DataGridView();
            this.BridgeClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedianSlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispersionSlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedianModerate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispersionModerate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedianExtensive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispersionExtensive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedianComplete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispersionComplete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BridgeClassesLbl = new System.Windows.Forms.Label();
            this.IMMesureLbl = new System.Windows.Forms.Label();
            this.IMMesureDrpDwn = new System.Windows.Forms.ComboBox();
            this.HelpTab = new System.Windows.Forms.TabPage();
            this.HelpRchTxt = new System.Windows.Forms.RichTextBox();
            this.msgClearBtn = new System.Windows.Forms.Button();
            this.convertProgressBar = new System.Windows.Forms.ProgressBar();
            this.convertRunBtn = new System.Windows.Forms.Button();
            this.inputFileOpnDlg = new System.Windows.Forms.OpenFileDialog();
            this.outputFolderOpnDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.MainTabCtrl.SuspendLayout();
            this.MainTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.assumptionGrp.SuspendLayout();
            this.BridgeFragilityTab.SuspendLayout();
            this.OtherClassesGrpBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BridgeFragilityGrdViw)).BeginInit();
            this.HelpTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputFileTxt
            // 
            this.inputFileTxt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputFileTxt.Location = new System.Drawing.Point(132, 48);
            this.inputFileTxt.Margin = new System.Windows.Forms.Padding(4);
            this.inputFileTxt.Name = "inputFileTxt";
            this.inputFileTxt.Size = new System.Drawing.Size(342, 27);
            this.inputFileTxt.TabIndex = 0;
            this.inputFileTxt.Text = "C:\\Users\\UR\\Desktop\\test.txt";
            // 
            // inputFileLbl
            // 
            this.inputFileLbl.AutoSize = true;
            this.inputFileLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputFileLbl.Location = new System.Drawing.Point(28, 48);
            this.inputFileLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.inputFileLbl.Name = "inputFileLbl";
            this.inputFileLbl.Size = new System.Drawing.Size(96, 19);
            this.inputFileLbl.TabIndex = 1;
            this.inputFileLbl.Text = "NBI File Path:";
            // 
            // MainTabCtrl
            // 
            this.MainTabCtrl.Controls.Add(this.MainTab);
            this.MainTabCtrl.Controls.Add(this.BridgeFragilityTab);
            this.MainTabCtrl.Controls.Add(this.HelpTab);
            this.MainTabCtrl.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTabCtrl.Location = new System.Drawing.Point(4, 3);
            this.MainTabCtrl.Margin = new System.Windows.Forms.Padding(4);
            this.MainTabCtrl.Name = "MainTabCtrl";
            this.MainTabCtrl.SelectedIndex = 0;
            this.MainTabCtrl.Size = new System.Drawing.Size(760, 537);
            this.MainTabCtrl.TabIndex = 2;
            // 
            // MainTab
            // 
            this.MainTab.BackColor = System.Drawing.Color.Gainsboro;
            this.MainTab.Controls.Add(this.msgRchTxt1);
            this.MainTab.Controls.Add(this.outputFolderOpnBtn);
            this.MainTab.Controls.Add(this.outputFolderLbl);
            this.MainTab.Controls.Add(this.outputFolderTxt);
            this.MainTab.Controls.Add(this.inputFileOpnBtn);
            this.MainTab.Controls.Add(this.inputFileTxt);
            this.MainTab.Controls.Add(this.inputFileLbl);
            this.MainTab.Controls.Add(this.groupBox1);
            this.MainTab.Controls.Add(this.assumptionGrp);
            this.MainTab.Location = new System.Drawing.Point(4, 27);
            this.MainTab.Margin = new System.Windows.Forms.Padding(4);
            this.MainTab.Name = "MainTab";
            this.MainTab.Padding = new System.Windows.Forms.Padding(4);
            this.MainTab.Size = new System.Drawing.Size(752, 506);
            this.MainTab.TabIndex = 0;
            this.MainTab.Text = "Bridge Characteristics";
            // 
            // msgRchTxt1
            // 
            this.msgRchTxt1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.msgRchTxt1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgRchTxt1.Location = new System.Drawing.Point(24, 343);
            this.msgRchTxt1.Name = "msgRchTxt1";
            this.msgRchTxt1.ReadOnly = true;
            this.msgRchTxt1.Size = new System.Drawing.Size(683, 162);
            this.msgRchTxt1.TabIndex = 15;
            this.msgRchTxt1.Text = resources.GetString("msgRchTxt1.Text");
            // 
            // outputFolderOpnBtn
            // 
            this.outputFolderOpnBtn.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputFolderOpnBtn.Location = new System.Drawing.Point(506, 200);
            this.outputFolderOpnBtn.Name = "outputFolderOpnBtn";
            this.outputFolderOpnBtn.Size = new System.Drawing.Size(39, 29);
            this.outputFolderOpnBtn.TabIndex = 10;
            this.outputFolderOpnBtn.Text = "...";
            this.outputFolderOpnBtn.UseVisualStyleBackColor = true;
            this.outputFolderOpnBtn.Click += new System.EventHandler(this.outputFolderOpnBtn_Click);
            // 
            // outputFolderLbl
            // 
            this.outputFolderLbl.AutoSize = true;
            this.outputFolderLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputFolderLbl.Location = new System.Drawing.Point(28, 201);
            this.outputFolderLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.outputFolderLbl.Name = "outputFolderLbl";
            this.outputFolderLbl.Size = new System.Drawing.Size(135, 19);
            this.outputFolderLbl.TabIndex = 9;
            this.outputFolderLbl.Text = "Output Folder Path:";
            // 
            // outputFolderTxt
            // 
            this.outputFolderTxt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputFolderTxt.Location = new System.Drawing.Point(165, 201);
            this.outputFolderTxt.Margin = new System.Windows.Forms.Padding(4);
            this.outputFolderTxt.Name = "outputFolderTxt";
            this.outputFolderTxt.Size = new System.Drawing.Size(342, 27);
            this.outputFolderTxt.TabIndex = 8;
            // 
            // inputFileOpnBtn
            // 
            this.inputFileOpnBtn.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputFileOpnBtn.Location = new System.Drawing.Point(472, 47);
            this.inputFileOpnBtn.Name = "inputFileOpnBtn";
            this.inputFileOpnBtn.Size = new System.Drawing.Size(39, 29);
            this.inputFileOpnBtn.TabIndex = 7;
            this.inputFileOpnBtn.Text = "...";
            this.inputFileOpnBtn.UseVisualStyleBackColor = true;
            this.inputFileOpnBtn.Click += new System.EventHandler(this.inputFileOpnBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AllCulvertRB);
            this.groupBox1.Controls.Add(this.NonCulvertRB);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(307, 101);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 70);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // AllCulvertRB
            // 
            this.AllCulvertRB.AutoSize = true;
            this.AllCulvertRB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllCulvertRB.Location = new System.Drawing.Point(0, 46);
            this.AllCulvertRB.Name = "AllCulvertRB";
            this.AllCulvertRB.Size = new System.Drawing.Size(230, 23);
            this.AllCulvertRB.TabIndex = 11;
            this.AllCulvertRB.TabStop = true;
            this.AllCulvertRB.Text = "Culvert and non-culvert bridges";
            this.AllCulvertRB.UseVisualStyleBackColor = true;
            // 
            // NonCulvertRB
            // 
            this.NonCulvertRB.AutoSize = true;
            this.NonCulvertRB.Checked = true;
            this.NonCulvertRB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NonCulvertRB.Location = new System.Drawing.Point(0, 21);
            this.NonCulvertRB.Name = "NonCulvertRB";
            this.NonCulvertRB.Size = new System.Drawing.Size(154, 23);
            this.NonCulvertRB.TabIndex = 12;
            this.NonCulvertRB.TabStop = true;
            this.NonCulvertRB.Text = "Non-culvert bridges";
            this.NonCulvertRB.UseVisualStyleBackColor = true;
            // 
            // assumptionGrp
            // 
            this.assumptionGrp.Controls.Add(this.label2);
            this.assumptionGrp.Controls.Add(this.assumptionAllRdb);
            this.assumptionGrp.Controls.Add(this.assumptionOnSystemRdb);
            this.assumptionGrp.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assumptionGrp.Location = new System.Drawing.Point(32, 101);
            this.assumptionGrp.Name = "assumptionGrp";
            this.assumptionGrp.Size = new System.Drawing.Size(342, 86);
            this.assumptionGrp.TabIndex = 13;
            this.assumptionGrp.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(129, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 19);
            this.label2.TabIndex = 16;
            this.label2.Text = "Bridge Considerations";
            // 
            // assumptionAllRdb
            // 
            this.assumptionAllRdb.AutoSize = true;
            this.assumptionAllRdb.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assumptionAllRdb.Location = new System.Drawing.Point(0, 46);
            this.assumptionAllRdb.Name = "assumptionAllRdb";
            this.assumptionAllRdb.Size = new System.Drawing.Size(207, 23);
            this.assumptionAllRdb.TabIndex = 11;
            this.assumptionAllRdb.TabStop = true;
            this.assumptionAllRdb.Text = "On- and -off system bridges";
            this.assumptionAllRdb.UseVisualStyleBackColor = true;
            // 
            // assumptionOnSystemRdb
            // 
            this.assumptionOnSystemRdb.AutoSize = true;
            this.assumptionOnSystemRdb.Checked = true;
            this.assumptionOnSystemRdb.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assumptionOnSystemRdb.Location = new System.Drawing.Point(0, 21);
            this.assumptionOnSystemRdb.Name = "assumptionOnSystemRdb";
            this.assumptionOnSystemRdb.Size = new System.Drawing.Size(149, 23);
            this.assumptionOnSystemRdb.TabIndex = 12;
            this.assumptionOnSystemRdb.TabStop = true;
            this.assumptionOnSystemRdb.Text = "On-system bridges";
            this.assumptionOnSystemRdb.UseVisualStyleBackColor = true;
            // 
            // BridgeFragilityTab
            // 
            this.BridgeFragilityTab.BackColor = System.Drawing.Color.Gainsboro;
            this.BridgeFragilityTab.Controls.Add(this.SkewConsidirationCKB);
            this.BridgeFragilityTab.Controls.Add(this.label1);
            this.BridgeFragilityTab.Controls.Add(this.OtherClassesGrpBox);
            this.BridgeFragilityTab.Controls.Add(this.CompleteLbl);
            this.BridgeFragilityTab.Controls.Add(this.msgRchTxt);
            this.BridgeFragilityTab.Controls.Add(this.ExtensiveLbl);
            this.BridgeFragilityTab.Controls.Add(this.ModerateLbl);
            this.BridgeFragilityTab.Controls.Add(this.SlightLbl);
            this.BridgeFragilityTab.Controls.Add(this.BridgeFragilityGrdViw);
            this.BridgeFragilityTab.Controls.Add(this.BridgeClassesLbl);
            this.BridgeFragilityTab.Controls.Add(this.IMMesureLbl);
            this.BridgeFragilityTab.Controls.Add(this.IMMesureDrpDwn);
            this.BridgeFragilityTab.Location = new System.Drawing.Point(4, 27);
            this.BridgeFragilityTab.Margin = new System.Windows.Forms.Padding(4);
            this.BridgeFragilityTab.Name = "BridgeFragilityTab";
            this.BridgeFragilityTab.Padding = new System.Windows.Forms.Padding(4);
            this.BridgeFragilityTab.Size = new System.Drawing.Size(752, 506);
            this.BridgeFragilityTab.TabIndex = 1;
            this.BridgeFragilityTab.Text = "Bridge Fragility";
            // 
            // SkewConsidirationCKB
            // 
            this.SkewConsidirationCKB.AutoSize = true;
            this.SkewConsidirationCKB.Location = new System.Drawing.Point(165, 320);
            this.SkewConsidirationCKB.Name = "SkewConsidirationCKB";
            this.SkewConsidirationCKB.Size = new System.Drawing.Size(15, 14);
            this.SkewConsidirationCKB.TabIndex = 16;
            this.SkewConsidirationCKB.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(23, 317);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 19);
            this.label1.TabIndex = 15;
            this.label1.Text = "Skew considiration";
            // 
            // OtherClassesGrpBox
            // 
            this.OtherClassesGrpBox.Controls.Add(this.CustomAndHazusRB);
            this.OtherClassesGrpBox.Controls.Add(this.AllCustomRB);
            this.OtherClassesGrpBox.Controls.Add(this.AllHazusRB);
            this.OtherClassesGrpBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OtherClassesGrpBox.Location = new System.Drawing.Point(14, 7);
            this.OtherClassesGrpBox.Name = "OtherClassesGrpBox";
            this.OtherClassesGrpBox.Size = new System.Drawing.Size(718, 78);
            this.OtherClassesGrpBox.TabIndex = 14;
            this.OtherClassesGrpBox.TabStop = false;
            this.OtherClassesGrpBox.Text = "Fragility Consideration";
            // 
            // CustomAndHazusRB
            // 
            this.CustomAndHazusRB.AutoSize = true;
            this.CustomAndHazusRB.Checked = true;
            this.CustomAndHazusRB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomAndHazusRB.Location = new System.Drawing.Point(174, 26);
            this.CustomAndHazusRB.Name = "CustomAndHazusRB";
            this.CustomAndHazusRB.Size = new System.Drawing.Size(544, 23);
            this.CustomAndHazusRB.TabIndex = 13;
            this.CustomAndHazusRB.TabStop = true;
            this.CustomAndHazusRB.Text = "Texas-specific fragility models for common bridge classes and HAZUS for others";
            this.CustomAndHazusRB.UseVisualStyleBackColor = true;
            // 
            // AllCustomRB
            // 
            this.AllCustomRB.AutoSize = true;
            this.AllCustomRB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllCustomRB.Location = new System.Drawing.Point(174, 55);
            this.AllCustomRB.Name = "AllCustomRB";
            this.AllCustomRB.Size = new System.Drawing.Size(519, 23);
            this.AllCustomRB.TabIndex = 11;
            this.AllCustomRB.TabStop = true;
            this.AllCustomRB.Text = "Texas-specific fragility models for common bridge classes and ignore others";
            this.AllCustomRB.UseVisualStyleBackColor = true;
            // 
            // AllHazusRB
            // 
            this.AllHazusRB.AutoSize = true;
            this.AllHazusRB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllHazusRB.Location = new System.Drawing.Point(174, -3);
            this.AllHazusRB.Name = "AllHazusRB";
            this.AllHazusRB.Size = new System.Drawing.Size(176, 23);
            this.AllHazusRB.TabIndex = 12;
            this.AllHazusRB.Text = "HAZUS fragility models";
            this.AllHazusRB.UseVisualStyleBackColor = true;
            this.AllHazusRB.CheckedChanged += new System.EventHandler(this.AllHazusRB_CheckedChanged);
            // 
            // CompleteLbl
            // 
            this.CompleteLbl.BackColor = System.Drawing.Color.White;
            this.CompleteLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CompleteLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompleteLbl.Location = new System.Drawing.Point(566, 125);
            this.CompleteLbl.Name = "CompleteLbl";
            this.CompleteLbl.Size = new System.Drawing.Size(141, 24);
            this.CompleteLbl.TabIndex = 8;
            this.CompleteLbl.Text = "Complete";
            this.CompleteLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // msgRchTxt
            // 
            this.msgRchTxt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.msgRchTxt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgRchTxt.Location = new System.Drawing.Point(24, 343);
            this.msgRchTxt.Name = "msgRchTxt";
            this.msgRchTxt.ReadOnly = true;
            this.msgRchTxt.Size = new System.Drawing.Size(683, 162);
            this.msgRchTxt.TabIndex = 14;
            this.msgRchTxt.Text = resources.GetString("msgRchTxt.Text");
            // 
            // ExtensiveLbl
            // 
            this.ExtensiveLbl.BackColor = System.Drawing.Color.White;
            this.ExtensiveLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ExtensiveLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtensiveLbl.Location = new System.Drawing.Point(426, 125);
            this.ExtensiveLbl.Name = "ExtensiveLbl";
            this.ExtensiveLbl.Size = new System.Drawing.Size(140, 24);
            this.ExtensiveLbl.TabIndex = 7;
            this.ExtensiveLbl.Text = "Extensive";
            this.ExtensiveLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ModerateLbl
            // 
            this.ModerateLbl.BackColor = System.Drawing.Color.White;
            this.ModerateLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ModerateLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModerateLbl.Location = new System.Drawing.Point(285, 125);
            this.ModerateLbl.Name = "ModerateLbl";
            this.ModerateLbl.Size = new System.Drawing.Size(141, 24);
            this.ModerateLbl.TabIndex = 6;
            this.ModerateLbl.Text = "Moderate";
            this.ModerateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SlightLbl
            // 
            this.SlightLbl.BackColor = System.Drawing.Color.White;
            this.SlightLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SlightLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlightLbl.Location = new System.Drawing.Point(144, 125);
            this.SlightLbl.Name = "SlightLbl";
            this.SlightLbl.Size = new System.Drawing.Size(141, 24);
            this.SlightLbl.TabIndex = 5;
            this.SlightLbl.Text = "Slight";
            this.SlightLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BridgeFragilityGrdViw
            // 
            this.BridgeFragilityGrdViw.AllowUserToAddRows = false;
            this.BridgeFragilityGrdViw.AllowUserToDeleteRows = false;
            this.BridgeFragilityGrdViw.AllowUserToResizeColumns = false;
            this.BridgeFragilityGrdViw.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BridgeFragilityGrdViw.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.BridgeFragilityGrdViw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.BridgeFragilityGrdViw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BridgeClass,
            this.MedianSlight,
            this.DispersionSlight,
            this.MedianModerate,
            this.DispersionModerate,
            this.MedianExtensive,
            this.DispersionExtensive,
            this.MedianComplete,
            this.DispersionComplete});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BridgeFragilityGrdViw.DefaultCellStyle = dataGridViewCellStyle14;
            this.BridgeFragilityGrdViw.Location = new System.Drawing.Point(24, 149);
            this.BridgeFragilityGrdViw.Name = "BridgeFragilityGrdViw";
            this.BridgeFragilityGrdViw.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.BridgeFragilityGrdViw.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.BridgeFragilityGrdViw.Size = new System.Drawing.Size(683, 157);
            this.BridgeFragilityGrdViw.TabIndex = 4;
            // 
            // BridgeClass
            // 
            this.BridgeClass.HeaderText = "BridgeClass";
            this.BridgeClass.Name = "BridgeClass";
            this.BridgeClass.ReadOnly = true;
            this.BridgeClass.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BridgeClass.Width = 80;
            // 
            // MedianSlight
            // 
            this.MedianSlight.HeaderText = "Median";
            this.MedianSlight.Name = "MedianSlight";
            this.MedianSlight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MedianSlight.Width = 70;
            // 
            // DispersionSlight
            // 
            this.DispersionSlight.HeaderText = "Dispersion";
            this.DispersionSlight.Name = "DispersionSlight";
            this.DispersionSlight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DispersionSlight.Width = 70;
            // 
            // MedianModerate
            // 
            this.MedianModerate.HeaderText = "Median";
            this.MedianModerate.Name = "MedianModerate";
            this.MedianModerate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MedianModerate.Width = 70;
            // 
            // DispersionModerate
            // 
            this.DispersionModerate.HeaderText = "Dispersion";
            this.DispersionModerate.Name = "DispersionModerate";
            this.DispersionModerate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DispersionModerate.Width = 70;
            // 
            // MedianExtensive
            // 
            this.MedianExtensive.HeaderText = "Median";
            this.MedianExtensive.Name = "MedianExtensive";
            this.MedianExtensive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MedianExtensive.Width = 70;
            // 
            // DispersionExtensive
            // 
            this.DispersionExtensive.HeaderText = "Dispersion";
            this.DispersionExtensive.Name = "DispersionExtensive";
            this.DispersionExtensive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DispersionExtensive.Width = 70;
            // 
            // MedianComplete
            // 
            this.MedianComplete.HeaderText = "Median";
            this.MedianComplete.Name = "MedianComplete";
            this.MedianComplete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MedianComplete.Width = 70;
            // 
            // DispersionComplete
            // 
            this.DispersionComplete.HeaderText = "Dispersion";
            this.DispersionComplete.Name = "DispersionComplete";
            this.DispersionComplete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DispersionComplete.Width = 70;
            // 
            // BridgeClassesLbl
            // 
            this.BridgeClassesLbl.AutoSize = true;
            this.BridgeClassesLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BridgeClassesLbl.Location = new System.Drawing.Point(23, 95);
            this.BridgeClassesLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.BridgeClassesLbl.Name = "BridgeClassesLbl";
            this.BridgeClassesLbl.Size = new System.Drawing.Size(210, 19);
            this.BridgeClassesLbl.TabIndex = 3;
            this.BridgeClassesLbl.Text = "Texas-specific fragility models";
            // 
            // IMMesureLbl
            // 
            this.IMMesureLbl.AutoSize = true;
            this.IMMesureLbl.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMMesureLbl.Location = new System.Drawing.Point(324, 95);
            this.IMMesureLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IMMesureLbl.Name = "IMMesureLbl";
            this.IMMesureLbl.Size = new System.Drawing.Size(130, 19);
            this.IMMesureLbl.TabIndex = 2;
            this.IMMesureLbl.Text = "Intensity Measure:";
            // 
            // IMMesureDrpDwn
            // 
            this.IMMesureDrpDwn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IMMesureDrpDwn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMMesureDrpDwn.FormattingEnabled = true;
            this.IMMesureDrpDwn.Items.AddRange(new object[] {
            "PGA (%g)",
            "PGV (cm/s)",
            "PGD (cm)",
            "PSA(0.2s) (%g)",
            "PSA(1.0s) (%g)",
            "PSA(Tn) (%g)",
            "Ia (cm/s)"});
            this.IMMesureDrpDwn.Location = new System.Drawing.Point(461, 92);
            this.IMMesureDrpDwn.Name = "IMMesureDrpDwn";
            this.IMMesureDrpDwn.Size = new System.Drawing.Size(128, 27);
            this.IMMesureDrpDwn.TabIndex = 0;
            this.IMMesureDrpDwn.SelectedIndexChanged += new System.EventHandler(this.IMMesureDrpDwn_SelectedIndexChanged);
            // 
            // HelpTab
            // 
            this.HelpTab.BackColor = System.Drawing.Color.Gainsboro;
            this.HelpTab.Controls.Add(this.HelpRchTxt);
            this.HelpTab.Location = new System.Drawing.Point(4, 27);
            this.HelpTab.Name = "HelpTab";
            this.HelpTab.Padding = new System.Windows.Forms.Padding(3);
            this.HelpTab.Size = new System.Drawing.Size(752, 506);
            this.HelpTab.TabIndex = 2;
            this.HelpTab.Text = "Help";
            // 
            // HelpRchTxt
            // 
            this.HelpRchTxt.BackColor = System.Drawing.Color.White;
            this.HelpRchTxt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpRchTxt.Location = new System.Drawing.Point(6, 6);
            this.HelpRchTxt.Name = "HelpRchTxt";
            this.HelpRchTxt.ReadOnly = true;
            this.HelpRchTxt.Size = new System.Drawing.Size(740, 340);
            this.HelpRchTxt.TabIndex = 0;
            this.HelpRchTxt.Text = resources.GetString("HelpRchTxt.Text");
            // 
            // msgClearBtn
            // 
            this.msgClearBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgClearBtn.Location = new System.Drawing.Point(691, 545);
            this.msgClearBtn.Name = "msgClearBtn";
            this.msgClearBtn.Size = new System.Drawing.Size(69, 26);
            this.msgClearBtn.TabIndex = 15;
            this.msgClearBtn.Text = "Clear";
            this.msgClearBtn.UseVisualStyleBackColor = true;
            this.msgClearBtn.Click += new System.EventHandler(this.msgClearBtn_Click);
            // 
            // convertProgressBar
            // 
            this.convertProgressBar.Location = new System.Drawing.Point(138, 547);
            this.convertProgressBar.Name = "convertProgressBar";
            this.convertProgressBar.Size = new System.Drawing.Size(547, 23);
            this.convertProgressBar.Step = 1;
            this.convertProgressBar.TabIndex = 3;
            // 
            // convertRunBtn
            // 
            this.convertRunBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertRunBtn.Location = new System.Drawing.Point(5, 540);
            this.convertRunBtn.Margin = new System.Windows.Forms.Padding(4);
            this.convertRunBtn.Name = "convertRunBtn";
            this.convertRunBtn.Size = new System.Drawing.Size(126, 41);
            this.convertRunBtn.TabIndex = 2;
            this.convertRunBtn.Text = "Run";
            this.convertRunBtn.UseVisualStyleBackColor = true;
            this.convertRunBtn.Click += new System.EventHandler(this.convertRunBtn_Click);
            // 
            // inputFileOpnDlg
            // 
            this.inputFileOpnDlg.Title = "Choose Input File Path";
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(766, 585);
            this.Controls.Add(this.msgClearBtn);
            this.Controls.Add(this.MainTabCtrl);
            this.Controls.Add(this.convertProgressBar);
            this.Controls.Add(this.convertRunBtn);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "TexasBridgeDataConvertor-V1.0";
            this.MainTabCtrl.ResumeLayout(false);
            this.MainTab.ResumeLayout(false);
            this.MainTab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.assumptionGrp.ResumeLayout(false);
            this.assumptionGrp.PerformLayout();
            this.BridgeFragilityTab.ResumeLayout(false);
            this.BridgeFragilityTab.PerformLayout();
            this.OtherClassesGrpBox.ResumeLayout(false);
            this.OtherClassesGrpBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BridgeFragilityGrdViw)).EndInit();
            this.HelpTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox inputFileTxt;
        private System.Windows.Forms.Label inputFileLbl;
        private System.Windows.Forms.TabControl MainTabCtrl;
        private System.Windows.Forms.TabPage MainTab;
        private System.Windows.Forms.TabPage BridgeFragilityTab;
        private System.Windows.Forms.Button convertRunBtn;
        private System.Windows.Forms.ProgressBar convertProgressBar;
        private System.Windows.Forms.OpenFileDialog inputFileOpnDlg;
        private System.Windows.Forms.Button inputFileOpnBtn;
        private System.Windows.Forms.Button outputFolderOpnBtn;
        private System.Windows.Forms.TextBox outputFolderTxt;
        private System.Windows.Forms.Label outputFolderLbl;
        private System.Windows.Forms.FolderBrowserDialog outputFolderOpnDlg;
        private System.Windows.Forms.GroupBox assumptionGrp;
        private System.Windows.Forms.RadioButton assumptionAllRdb;
        private System.Windows.Forms.RadioButton assumptionOnSystemRdb;
        private System.Windows.Forms.RichTextBox msgRchTxt;
        private System.Windows.Forms.Button msgClearBtn;
        private System.Windows.Forms.ComboBox IMMesureDrpDwn;
        private System.Windows.Forms.DataGridView BridgeFragilityGrdViw;
        private System.Windows.Forms.Label BridgeClassesLbl;
        private System.Windows.Forms.Label IMMesureLbl;
        private System.Windows.Forms.Label CompleteLbl;
        private System.Windows.Forms.Label ExtensiveLbl;
        private System.Windows.Forms.Label ModerateLbl;
        private System.Windows.Forms.Label SlightLbl;
        private System.Windows.Forms.DataGridViewTextBoxColumn BridgeClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedianSlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispersionSlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedianModerate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispersionModerate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedianExtensive;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispersionExtensive;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedianComplete;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispersionComplete;
        private System.Windows.Forms.GroupBox OtherClassesGrpBox;
        private System.Windows.Forms.RadioButton AllCustomRB;
        private System.Windows.Forms.RadioButton AllHazusRB;
        private System.Windows.Forms.RadioButton CustomAndHazusRB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton AllCulvertRB;
        private System.Windows.Forms.RadioButton NonCulvertRB;
        private System.Windows.Forms.RichTextBox msgRchTxt1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox SkewConsidirationCKB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage HelpTab;
        private System.Windows.Forms.RichTextBox HelpRchTxt;
    }
}

