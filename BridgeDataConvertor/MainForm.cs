﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BridgeDataConvertor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            outputFolderTxt.Text = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            IMMesureDrpDwn.SelectedIndex = 0;
        }

        /* Main Analysis:
         * Main analysis (ie. coverting bridge data) is done when "Run" button is pressed. 
         * "convertRunBtn_Click" function runs when "Run" button is pressed and contains the main code to analysis.
         * It also uses helper functions define in this same file.
         * In all cases "custom class" is defined as the class assigned and used by Texas-specific model.
         * After checking input/output path, reading and cleaning the input raw data, analysis iterates over
         * each row in it and adds the result to a row in output table. 
         */
        private void convertRunBtn_Click(object sender, EventArgs e)
        {
            // Check if input/output file path is filled
            if (String.IsNullOrWhiteSpace(inputFileTxt.Text))
            {
                MessageBox.Show("Input file path can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (String.IsNullOrWhiteSpace(outputFolderTxt.Text))
            {
                MessageBox.Show("Output folder path can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else // continue if input/output path is filled
            {
                // Disable Run Button, intensity mesure combo box and fragility table at the start of analysis
                convertRunBtn.Enabled = false;
                IMMesureDrpDwn.Enabled = false;
                BridgeFragilityGrdViw.Enabled = false;
                BridgeFragilityGrdViw.ForeColor = SystemColors.GrayText;

                // Define intensity mesure name used in output table for Texas-specific models according to intensity mesure combo box value
                string CustomIMName = "Not Assigned";
                if (!AllHazusRB.Checked) // If only HAZUS model is checked, output will only contain PGA
                {
                    switch (IMMesureDrpDwn.SelectedItem.ToString())
                    {
                        case "PGA (%g)":
                            CustomIMName = "PGA";
                            break;
                        case "PGV (cm/s)":
                            CustomIMName = "PGV";
                            break;
                        case "PGD (cm)":
                            CustomIMName = "PGD";
                            break;
                        case "PSA(0.2s) (%g)":
                            CustomIMName = "PSA2";
                            break;
                        case "PSA(1.0s) (%g)":
                            CustomIMName = "PSA10";
                            break;
                        case "PSA(Tn) (%g)":
                            CustomIMName = "PSATn";
                            break;
                        case "Ia (cm/s)":
                            CustomIMName = "Ia";
                            break;
                        default:
                            CustomIMName = "Not Assigned";
                            break;
                    }
                }

                // Add text to rich textboxes
                RichTextboxSetText(msgRchTxt, "Analysing data started...\n");
                RichTextboxSetText(msgRchTxt1, "Analysing data started...\n");
                RichTextboxScrolltoEnd(msgRchTxt);
                RichTextboxScrolltoEnd(msgRchTxt1);


                // Use "Task.Run()" to run Analysis in a different thread than one controlling the interface
                // Otherwise interface changes during the analysis will not work correctly
                Task.Run(() =>
                {

                    // Read raw data
                    DataTable rawData = ConvertTxtFileToDataTable(inputFileTxt.Text, ',');

                    //Remove rows not containig necessary values
                    int initialNumBridge = rawData.Rows.Count;
                    var rowsToRemove = rawData.Select("LONG_017 = '' OR LONG_017 = '' OR MAIN_UNIT_SPANS_045 = '' OR APPR_SPANS_046 = '' OR " +
                        "DEGREES_SKEW_034 = '' OR STRUCTURE_LEN_MT_049 = '' OR MAX_SPAN_LEN_MT_048 = '' OR " +
                        "YEAR_BUILT_027 = '' OR STRUCTURE_KIND_043A = '' OR STRUCTURE_TYPE_043B = '' OR " +
                        "DECK_WIDTH_MT_052 = '' OR MAINTENANCE_021 = ''");
                    foreach (var row in rowsToRemove)
                        row.Delete();


                    int numRemovedBridges = initialNumBridge - rawData.Rows.Count;
                    RichTextboxSetText(msgRchTxt, numRemovedBridges + " bridges were removed due to lack of required information.\n");
                    RichTextboxSetText(msgRchTxt1, numRemovedBridges + " bridges were removed due to lack of required information.\n");

                    // Filter data based on bridge maintenance responsibility and being Culvert if related options are selected
                    if (assumptionOnSystemRdb.Checked)
                    {
                        rawData = rawData.Select("MAINTENANCE_021 = 1").CopyToDataTable();
                    }
                    if (NonCulvertRB.Checked)
                    {
                        rawData = rawData.Select("STRUCTURE_TYPE_043B <> 19").CopyToDataTable();
                    }

                    // Find and save IDs of bridges with special cases ( Those with latitude/longitude of 0 or 1000000, and/or toal span of 0) 
                    DataRow[] removedRows = rawData.Select("LONG_017 = 0 OR LONG_017 = 1000000 OR LAT_016 = 0 OR LAT_016 = 1000000");
                    removedRows.Concat(rawData.Select("(MAIN_UNIT_SPANS_045 + APPR_SPANS_046) = 0"));
                    int romvedRowsNumber = removedRows.Length;

                    // Show removed bridges IDs if any, in rich textboxes
                    if (romvedRowsNumber > 0)
                    {
                        RichTextboxSetText(msgRchTxt, "The bridges listed below do not have complete data and are removed from the output file.\nBridge ID of:\n");
                        RichTextboxSetText(msgRchTxt1, "The bridges listed below do not have complete data and are removed from the output file.\nBridge ID of:\n");
                        RichTextboxScrolltoEnd(msgRchTxt);
                        RichTextboxScrolltoEnd(msgRchTxt1);

                        string[] removedRowsIds = new string[romvedRowsNumber];
                        for (int r = 0; r < romvedRowsNumber; r++)
                        {
                            removedRowsIds[r] = removedRows[r]["STATE_CODE_001"] + "_" + removedRows[r]["STRUCTURE_NUMBER_008"];

                            RichTextboxSetText(msgRchTxt, removedRowsIds[r] + "\n");
                            RichTextboxSetText(msgRchTxt1, removedRowsIds[r] + "\n");
                            RichTextboxScrolltoEnd(msgRchTxt);
                            RichTextboxScrolltoEnd(msgRchTxt1);
                        }
                    }

                    // Remove incomplete rows 
                    rawData = rawData.Select("LONG_017 <> 1000000 AND LONG_017 <> 0 AND LAT_016 <> 0 AND LAT_016 <> 1000000").CopyToDataTable();
                    rawData = rawData.Select("(MAIN_UNIT_SPANS_045 + APPR_SPANS_046) <> 0").CopyToDataTable();



                    // Prepair progress bar 
                    ProgressBarSetValue(convertProgressBar, 0); // Set current value of progress bar to 0
                    int numberOfRows = rawData.Rows.Count; // Find number of rows in data
                    ProgressBarSetMax(convertProgressBar, numberOfRows); // Set maximum of progress bar to number of rows in data

                    // Create final DataTable and assign column names
                    DataTable finalTable = new DataTable();
                    string[] finalTableColumnNames = { "EXTERNAL_FACILITY_ID", "FACILITY_TYPE", "FACILITY_NAME", "SHORT_NAME", "DESCRIPTION", "LAT", "LON", "METRIC:PGA:GREEN", "METRIC:PGA:YELLOW", "METRIC:PGA:ORANGE", "METRIC:PGA:RED", "GREEN:METRIC", "GREEN:ALPHA", "GREEN:BETA", "YELLOW:METRIC", "YELLOW:ALPHA", "YELLOW:BETA", "ORANGE:METRIC", "ORANGE:ALPHA", "ORANGE:BETA", "RED:METRIC", "RED:ALPHA", "RED:BETA", "CustomClass" }; // CustomClass is used during the run but dropped at the end
                    //  Column indexes:                         0                       1                   2          3               4        5      6           7                   8                       9                       10              11          12              13              14              15              16              17              18              19              20          21          22           23           
                    foreach (var col in finalTableColumnNames)
                    {
                        finalTable.Columns.Add(col);
                    }

                    // Assing values to final DataTable by iterating over rawData rows
                    int i = 0;
                    foreach (DataRow row in rawData.Rows)
                    {
                        finalTable.Rows.Add();

                        //EXTERNAL_FACILITY_ID column
                        finalTable.Rows[i][0] = row["STATE_CODE_001"] + "_" + row["STRUCTURE_NUMBER_008"];

                        //FACILITY_TYPE column
                        finalTable.Rows[i][1] = "BRIDGE";

                        //FACILITY_NAME & SHORT_NAME columns
                        finalTable.Rows[i][2] = finalTable.Rows[i][3] = row["STRUCTURE_NUMBER_008"].ToString() + "_" + row["FEATURES_DESC_006A"];


                        //Lattitude column
                        string tmpLat = row["LAT_016"].ToString();
                        var tlal = tmpLat.Length;
                        finalTable.Rows[i][5] = (double.Parse(tmpLat.Substring(0, tlal - 6)) + double.Parse(tmpLat.Substring(tlal - 5 - 1, 2)) / 60 + double.Parse(tmpLat.Substring(tlal - 3 - 1, 4)) / 360000).ToString();


                        //Longitude column
                        string tmpLong = row["LONG_017"].ToString();
                        var tll = tmpLong.Length;
                        finalTable.Rows[i][6] = ((double.Parse(tmpLong.Substring(0, tll - 6)) + double.Parse(tmpLong.Substring(tll - 5 - 1, 2)) / 60 + double.Parse(tmpLong.Substring(tll - 3 - 1, 4)) / 360000) * (-1)).ToString();

                        // Assign NBI class code by using code_a and code_b
                        string code_a, code_b;
                        int raw_code_a = int.Parse(row["STRUCTURE_KIND_043A"].ToString());
                        if (0 <= raw_code_a & raw_code_a <= 9)
                        {
                            code_a = raw_code_a.ToString();
                        }
                        else
                        {
                            code_a = "100";
                        }

                        int raw_code_b = int.Parse(row["STRUCTURE_TYPE_043B"].ToString());
                        if (0 <= raw_code_b & raw_code_b <= 22)
                        {
                            code_b = raw_code_b.ToString();
                            // code_b should be double digits
                            if (code_b.Length == 1)
                            {
                                code_b = "0" + code_b;
                            }
                        }
                        else
                        {
                            code_b = "00";
                        }
                        string NBIClassCode = code_a + code_b;


                        // Assign custom class according to NBI class code
                        string CustomClass;

                        switch (NBIClassCode.ToString())
                        {
                            case "402":
                                CustomClass = "MCSTEEL";
                                break;
                            case "403":
                                CustomClass = "MCSTEEL";
                                break;
                            case "302":
                                CustomClass = "MSSTEEL";
                                break;
                            case "303":
                                CustomClass = "MSSTEEL";
                                break;
                            case "502":
                                CustomClass = "MSPC";
                                break;
                            case "503":
                                CustomClass = "MSPC";
                                break;
                            case "102":
                                CustomClass = "MSRC";
                                break;
                            case "103":
                                CustomClass = "MSRC";
                                break;
                            case "101":
                                CustomClass = "MSRC-Slab";
                                break;
                            case "201":
                                CustomClass = "MCRC-Slab";
                                break;
                            default:
                                CustomClass = "Unknown";
                                break;
                        }


                        // Save custom class into the table, temporarily. It will be used after the iteration over the rows is done and is dropped before saving the output table.
                        finalTable.Rows[i][23] = CustomClass;

                        // Assing values needed from source data for assigning HAZUS class, to temporary variables
                        string HAZUSClass = "Not Assigned";
                        double tmpStructureLength_m = double.Parse(row["STRUCTURE_LEN_MT_049"].ToString());
                        double tmpSpanLength = double.Parse(row["MAX_SPAN_LEN_MT_048"].ToString());
                        int tmpSpanNumber = int.Parse(row["MAIN_UNIT_SPANS_045"].ToString()) + int.Parse(row["APPR_SPANS_046"].ToString());
                        int tmpConstuctionYear = int.Parse(row["YEAR_BUILT_027"].ToString());

                        //Define Hazus Bridges NBI class code collection
                        string[] rcNBIClasses = { "101", "102", "103", "104", "105", "106" };
                        string[] rcContNBIClasses = { "201", "202", "203", "204", "205", "206" };
                        string[] steelNBIClasses = { "301", "302", "303", "304", "305", "306" };
                        string[] steelContNBIClasses = { "401", "402", "403", "404", "405", "406", "407", "408", "409", "410" };
                        string[] psConcreteNBIClasses = { "501", "502", "503", "504", "505", "506" };
                        string[] psConcreteContClasses = { "601", "602", "603", "604", "605", "606" };

                        // Assing HAZUS Class
                        if (tmpSpanLength > 150) //major bridge
                        {
                            HAZUSClass = CheckSeismicConsideration("HWB1", "HWB2", tmpConstuctionYear);
                        }
                        else if (tmpSpanLength <= 150)
                        {
                            if (tmpSpanNumber == 1)
                            {
                                HAZUSClass = CheckSeismicConsideration("HWB3", "HWB4", tmpConstuctionYear);
                            }
                            else if (tmpSpanNumber > 1)
                            {
                                if (rcNBIClasses.Contains(NBIClassCode)) //RC
                                {
                                    HAZUSClass = CheckSeismicConsideration("HWB5", "HWB7", tmpConstuctionYear);
                                }
                                else if (rcContNBIClasses.Contains(NBIClassCode)) //RC_Cont
                                {
                                    HAZUSClass = CheckSeismicConsideration("HWB10", "HWB11", tmpConstuctionYear);
                                }
                                else if (steelNBIClasses.Contains(NBIClassCode)) //Steel
                                {
                                    if (tmpStructureLength_m >= 20)
                                    {
                                        HAZUSClass = CheckSeismicConsideration("HWB12", "HWB14", tmpConstuctionYear);
                                    }
                                    else if (tmpStructureLength_m < 20)
                                    {
                                        HAZUSClass = CheckSeismicConsideration("HWB24", "HWB14", tmpConstuctionYear);
                                    }
                                }
                                else if (steelContNBIClasses.Contains(NBIClassCode)) //Steel_Cont
                                {
                                    if (tmpStructureLength_m >= 20)
                                    {
                                        HAZUSClass = CheckSeismicConsideration("HWB15", "HWB16", tmpConstuctionYear);
                                    }
                                    else if (tmpStructureLength_m < 20)
                                    {
                                        HAZUSClass = CheckSeismicConsideration("HWB26", "HWB16", tmpConstuctionYear);
                                    }
                                }
                                else if (psConcreteNBIClasses.Contains(NBIClassCode)) //PS Concrete
                                {
                                    HAZUSClass = CheckSeismicConsideration("HWB17", "HWB19", tmpConstuctionYear);
                                }
                                else if (psConcreteContClasses.Contains(NBIClassCode))//PS Concrete_Cont
                                {
                                    HAZUSClass = CheckSeismicConsideration("HWB22", "HWB23", tmpConstuctionYear);
                                }
                                else
                                {
                                    HAZUSClass = "HWB28";
                                }

                            }
                        }

                        // Get HAZUS values for PGA according to assigned class
                        string[] hazusValues = HAZUSValues(HAZUSClass);

                        // Apply skew consideration if checked
                        if (SkewConsidirationCKB.Checked)
                        {
                            double[] HAZUS_skews = HAZUS_Skew_Values(row["DEGREES_SKEW_034"].ToString(), HAZUSClass); // Get consideration values according to class
                            hazusValues[0] = (double.Parse(hazusValues[0]) * HAZUS_skews[0]).ToString();
                            hazusValues[1] = (double.Parse(hazusValues[1]) * HAZUS_skews[1]).ToString();
                            hazusValues[2] = (double.Parse(hazusValues[2]) * HAZUS_skews[2]).ToString();
                            hazusValues[3] = (double.Parse(hazusValues[3]) * HAZUS_skews[3]).ToString();
                        }

                        // Assing default PGA values to output table (These values are assinged from HAZUS values regardless of selected intesity mesure)
                        finalTable.Rows[i][7] = hazusValues[0];
                        finalTable.Rows[i][8] = hazusValues[1];
                        finalTable.Rows[i][9] = hazusValues[2];
                        finalTable.Rows[i][10] = hazusValues[3];

                        // Value of description_class variable is used in description column
                        string description_class;

                        //Assign non-default valuse according to selected options
                        if (AllHazusRB.Checked | (CustomAndHazusRB.Checked & CustomClass == "Unknown"))
                        {
                            // Use HAZUS values for these columns as well, if only HAZUS models is checked or custom class of bridge is unknown
                            finalTable.Rows[i][11] = finalTable.Rows[i][14] = finalTable.Rows[i][17] = finalTable.Rows[i][20] = "PGA";
                            finalTable.Rows[i][13] = finalTable.Rows[i][16] = finalTable.Rows[i][19] = finalTable.Rows[i][22] = "0.60"; // All HAZUS classes have the same dispersion of 0.6
                            finalTable.Rows[i][12] = hazusValues[0];
                            finalTable.Rows[i][15] = hazusValues[1];
                            finalTable.Rows[i][18] = hazusValues[2];
                            finalTable.Rows[i][21] = hazusValues[3];

                            description_class = HAZUSClass;
                        }
                        else if (CustomClass != "Unknown") // This condition is needed for only custom option to work right. Do NOT remove it.
                        {
                            // If Texas-specific model is checked and custom class is known, assign values according to fragility table

                            // Assign intensity mesure name
                            finalTable.Rows[i][11] = finalTable.Rows[i][14] = finalTable.Rows[i][17] = finalTable.Rows[i][20] = CustomIMName;
                            string[] gridViewValues = new string[8];

                            // Find the row of fragility table according to custom class
                            int RowIndex = -1;
                            switch (CustomClass)
                            {
                                case "MCSTEEL":
                                    RowIndex = 0;
                                    break;
                                case "MSSTEEL":
                                    RowIndex = 1;
                                    break;
                                case "MSPC":
                                    RowIndex = 2;
                                    break;
                                case "MSRC":
                                    RowIndex = 3;
                                    break;
                                case "MCRC-Slab":
                                    RowIndex = 4;
                                    break;
                                case "MSRC-Slab":
                                    RowIndex = 5;
                                    break;

                                default:
                                    break;
                            }

                            // Assign slight, moderate, extensive and complete fragility values to variables
                            string slt = BridgeFragilityGrdViw.Rows[RowIndex].Cells[1].Value.ToString();
                            string med = BridgeFragilityGrdViw.Rows[RowIndex].Cells[3].Value.ToString();
                            string ext = BridgeFragilityGrdViw.Rows[RowIndex].Cells[5].Value.ToString();
                            string cmp = BridgeFragilityGrdViw.Rows[RowIndex].Cells[7].Value.ToString();

                            // Apply skew consideration if checked
                            if (SkewConsidirationCKB.Checked)
                            {
                                double[] customSkews = TexasSpecificSkewValues(row["DEGREES_SKEW_034"].ToString(), CustomClass);
                                slt = (double.Parse(slt) * customSkews[0]).ToString();
                                med = (double.Parse(med) * customSkews[1]).ToString();
                                ext = (double.Parse(ext) * customSkews[2]).ToString();
                                cmp = (double.Parse(cmp) * customSkews[3]).ToString();
                            }

                            // Assign final values to output table
                            finalTable.Rows[i][12] = slt;
                            finalTable.Rows[i][13] = BridgeFragilityGrdViw.Rows[RowIndex].Cells[2].Value;
                            finalTable.Rows[i][15] = med;
                            finalTable.Rows[i][16] = BridgeFragilityGrdViw.Rows[RowIndex].Cells[4].Value;
                            finalTable.Rows[i][18] = ext;
                            finalTable.Rows[i][19] = BridgeFragilityGrdViw.Rows[RowIndex].Cells[6].Value;
                            finalTable.Rows[i][21] = cmp;
                            finalTable.Rows[i][22] = BridgeFragilityGrdViw.Rows[RowIndex].Cells[8].Value;


                            description_class = CustomClass;
                        }
                        else
                        {
                            description_class = "Unknown";
                        }


                        //DESCRIPTION column
                        finalTable.Rows[i][4] = (int.Parse(row["MAIN_UNIT_SPANS_045"].ToString()) + int.Parse(row["APPR_SPANS_046"].ToString())).ToString() + "-span;" +
                        row["DEGREES_SKEW_034"] + " deg skew;" +
                        row["STRUCTURE_LEN_MT_049"] + " m structure Length;" +
                        row["MAX_SPAN_LEN_MT_048"] + " m Max Span Length;" +
                        " Built " + row["YEAR_BUILT_027"] + ";" +
                        description_class + ";";


                        // Show progress in progress bar
                        ProgressBarStep(convertProgressBar);
                        i++;
                    }

                    //Sort output table based on ID
                    finalTable.DefaultView.Sort = "EXTERNAL_FACILITY_ID ASC";
                    finalTable = finalTable.DefaultView.ToTable();

                    //Remove Rows with unknown custom class if only Texas-specific model option is checked
                    if (AllCustomRB.Checked)
                    {
                        try
                        {

                            finalTable = finalTable.Select("CustomClass <> 'Unknown'").CopyToDataTable();
                        }
                        catch (Exception expe)
                        {
                            MessageBox.Show(expe.Message);

                        }
                    }

                    //Drop temporary column
                    finalTable.Columns.Remove("CustomClass");

                    // Try writing output to file and show the error if there is any
                    try
                    {
                        WriteDataTableToCSV(finalTable, outputFolderTxt.Text + "\\" + Path.GetFileNameWithoutExtension(inputFileTxt.Text) + "_Converted" + ".csv");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n\n Please try again after fixing the error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        convertRunBtn.Enabled = true;

                        RichTextboxSetText(msgRchTxt, "Analysing finished with errors.\n_______________________________________\n");
                        RichTextboxSetText(msgRchTxt1, "Analysing finished with errors.\n_______________________________________\n");
                        RichTextboxScrolltoEnd(msgRchTxt);
                        RichTextboxScrolltoEnd(msgRchTxt1);

                        throw;
                    }


                    // Restore convert button, intensity mesure drop-down and fragility table to enabled state
                    convertRunBtn.Enabled = true;
                    IMMesureDrpDwn.Enabled = true;
                    BridgeFragilityGrdViw.Enabled = true;
                    BridgeFragilityGrdViw.ForeColor = Color.Black;

                    RichTextboxSetText(msgRchTxt, "Analysing finished.\n_________________________________________________\n");
                    RichTextboxSetText(msgRchTxt1, "Analysing finished.\n_________________________________________________\n");
                    RichTextboxScrolltoEnd(msgRchTxt);
                    RichTextboxScrolltoEnd(msgRchTxt1);
                });


            }
        }

        //Write the input file path to related textbox when file is chosen
        private void inputFileOpnBtn_Click(object sender, EventArgs e)
        {
            if (inputFileOpnDlg.ShowDialog() == DialogResult.OK)
            {
                inputFileTxt.Text = inputFileOpnDlg.FileName;
            }
            msgRchTxt.AppendText("Press Run Button.\n");
            msgRchTxt1.AppendText("Press Run Button.\n");
            RichTextboxScrolltoEnd(msgRchTxt);
            RichTextboxScrolltoEnd(msgRchTxt1);
        }

        //Write the output file path to related textbox when folder is chosen
        private void outputFolderOpnBtn_Click(object sender, EventArgs e)
        {
            if (outputFolderOpnDlg.ShowDialog() == DialogResult.OK)
            {
                outputFolderTxt.Text = outputFolderOpnDlg.SelectedPath;
            }

        }

        // Clear rich textboxes when clear button is clicked
        private void msgClearBtn_Click(object sender, EventArgs e)
        {
            msgRchTxt.Text = "";
            msgRchTxt1.Text = "";
            convertProgressBar.Value = 0;
        }

        // Disable fragility table when only HAZUS model is selected and enable otherwise
        private void AllHazusRB_CheckedChanged(object sender, EventArgs e)
        {
            if (AllHazusRB.Checked)
            {
                BridgeFragilityGrdViw.Enabled = false;
                IMMesureDrpDwn.Enabled = false;
                BridgeFragilityGrdViw.ForeColor = SystemColors.GrayText;
            }
            else
            {
                BridgeFragilityGrdViw.Enabled = true;
                IMMesureDrpDwn.Enabled = true;
                BridgeFragilityGrdViw.ForeColor = Color.Black; ;
            }
        }

        // Change values of fragility GridView when selected intensity mesure is changed
        // During the analysis, Texas-specefic values are read directly from the fragility GridView
        private void IMMesureDrpDwn_SelectedIndexChanged(object sender, EventArgs e)
        {
            BridgeFragilityGrdViw.Rows.Clear();
            string[] row1 = new string[8];
            string[] row2 = new string[8];
            string[] row3 = new string[8];
            string[] row4 = new string[8];
            string[] row5 = new string[8];
            string[] row6 = new string[8];
            object[] rows = new object[8];
            switch (IMMesureDrpDwn.SelectedItem.ToString())
            {
                case "PGA (%g)":
                    row1 = new[]{ "MCSTEEL",
                               "39", "0.90", "107", "0.84", "160", "0.80", "323", "0.82"};
                    row2 = new[]{"MSSTEEL",
                              "40", "0.69", "101", "0.64", "137", "0.65", "300", "0.75"};
                    row3 = new[]{"MSPC",
                               "36", "0.92", "122", "0.91", "173", "0.92", "237", "0.91"};
                    row4 = new[]{"MSRC",
                               "82", "0.78", "201", "0.83", "247", "0.89", "308", "0.91"};
                    row5 = new[]{"MCRC-Slab",
                               "93", "0.81", "217", "0.83", "300", "0.94", "423", "1.01"};
                    row6 = new[]{"MSRC-Slab",
                               "99", "0.66", "226", "0.69", "292", "0.78", "371", "0.80"};
                    break;

                case "PGV (cm/s)":
                    row1 = new[]{ "MCSTEEL",
                               "13.00", "0.73", "35.24", "0.72", "50.74", "0.65", "95.35", "0.61"};
                    row2 = new[]{"MSSTEEL",
                               "12.53", "0.54", "32.04", "0.51", "43.49", "0.54", "97.95", "0.61"};
                    row3 = new[]{"MSPC",
                               "11.49", "0.69", "36.07", "0.68", "49.92", "0.69", "67.49", "0.68"};
                    row4 = new[]{"MSRC",
                               "24.57", "0.61", "56.48", "0.64", "68.68", "0.70", "83.22", "0.71"};
                    row5 = new[]{"MCRC-Slab",
                               "28.62", "0.74", "67.18", "0.75", "90.18", "0.85", "122.43", "0.90"};
                    row6 = new[]{"MSRC-Slab",
                               "31.32", "0.58", "73.57", "0.61", "93.08", "0.70", "125.35", "0.75"};
                    break;

                case "PGD (cm)":
                    row1 = new[]{ "MCSTEEL",
                               "1.79", "2.01", "16.85", "1.91", "39.76", "1.88", "160.59", "1.82"};
                    row2 = new[]{"MSSTEEL",
                               "1.71", "1.80", "14.44", "1.82", "31.01", "1.85", "163.61", "1.84"};
                    row3 = new[]{"MSPC",
                               "19.85", "1.70", "19.62", "1.65", "37.56", "1.67", "67.83", "1.67"};
                    row4 = new[]{"MSRC",
                               "18.25", "1.99", "131.92", "2.00", "213.00", "2.10", "343.78", "2.13"};
                    row5 = new[]{"MCRC-Slab",
                               "28.93", "2.51", "253.45", "2.47", "501.96", "2.57", "951.99", "2.61"};
                    row6 = new[]{"MSRC-Slab",
                               "34.07", "2.03", "250.69", "2.01", "438.81", "2.12", "777.50", "2.15"};
                    break;

                case "PSA(0.2s) (%g)":
                    row1 = new[]{ "MCSTEEL",
                               "70", "0.84", "198", "0.76", "295", "0.73", "614", "0.77"};
                    row2 = new[]{"MSSTEEL",
                               "69", "0.62", "184", "0.55", "252", "0.57", "568", "0.68"};
                    row3 = new[]{"MSPC",
                               "60", "0.92", "221", "0.90", "320", "0.91", "452", "0.91"};
                    row4 = new[]{"MSRC",
                               "149", "0.70", "368", "0.75", "455", "0.81", "577", "0.85"};
                    row5 = new[]{"MCRC-Slab",
                               "167", "0.72", "376", "0.70", "490", "0.77", "642", "0.81"};
                    row6 = new[]{"MSRC-Slab",
                               "188", "0.63", "430", "0.62", "540", "0.70", "698", "0.73"};
                    break;

                case "PSA(1.0s) (%g)":
                    row1 = new[]{ "MCSTEEL",
                               "5", "1.04", "20", "0.95", "34", "0.92", "75", "0.79"};
                    row2 = new[]{"MSSTEEL",
                               "5", "0.94", "19", "0.92", "30", "0.94", "78", "0.86"};
                    row3 = new[]{"MSPC",
                               "5", "0.82", "21", "0.79", "32", "0.80", "47", "0.79"};
                    row4 = new[]{"MSRC",
                               "16", "1.00", "51", "0.99", "66", "1.07", "87", "1.08"};
                    row5 = new[]{"MCRC-Slab",
                               "21", "1.28", "72", "1.24", "106", "1.33", "152", "1.34"};
                    row6 = new[]{"MSRC-Slab",
                               "24", "1.06", "77", "1.03", "106", "1.11", "147", "1.13"};
                    break;

                case "PSA(Tn) (%g)":
                    row1 = new[]{ "MCSTEEL",
                               "20", "1.02", "78", "0.86", "126", "0.81", "298", "0.84"};
                    row2 = new[]{"MSSTEEL",
                               "36", "0.65", "113", "0.62", "164", "0.65", "417", "0.73"};
                    row3 = new[]{"MSPC",
                              "6", "1.28", "40", "1.25", "66", "1.26", "107", "1.25"};
                    row4 = new[]{"MSRC",
                               "120", "1.11", "411", "1.11", "559", "1.21", "765", "1.24"};
                    row5 = new[]{"MCRC-Slab",
                               "182", "0.95", "517", "0.98", "756", "1.11", "1138", "1.19"};
                    row6 = new[]{"MSRC-Slab",
                               "210", "0.91", "648", "0.96", "914", "1.08", "1249", "1.10"};
                    break;

                case "Ia (cm/s)":
                    row1 = new[]{ "MCSTEEL",
                                "131", "1.85", "1130", "1.80", "2603", "1.71", "25283", "2.4"};
                    row2 = new[]{"MSSTEEL",
                               "130", "1.46", "968", "1.41", "1896", "1.44", "10702", "1.65"};
                    row3 = new[]{"MSPC",
                              "119", "1.69", "1345", "1.68", "385644", "0.83", "5168", "1.71"};
                    row4 = new[]{"MSRC",
                               "707", "1.65", "4947", "1.77", "8052", "1.92", "13617", "2.00"};
                    row5 = new[]{"MCRC-Slab",
                               "835", "1.63", "4556", "1.65", "8212", "1.83", "15270", "1.94"};
                    row6 = new[]{"MSRC-Slab",
                             "1115", "1.43", "6771", "1.51", "11541", "1.69", "21069", "1.78"};
                    break;

                default:
                    break;
            }


            rows = new[] { row1, row2, row3, row4, row5, row6 };
            foreach (string[] rowArray in rows)
            {
                BridgeFragilityGrdViw.Rows.Add(rowArray);
            }
        }

        /// <summary>
        /// Retrieve HAZUS values according the bridge HAZUS class
        /// </summary>
        /// <param name="bridgeClass">Assigned HAZUS class of bridge</param>
        /// <returns>Array of 4 strings representing slight, moderate, extensive and complete values</returns>
        public string[] HAZUSValues(string bridgeClass)
        {
            string[] values = new string[4];
            switch (bridgeClass)
            {
                case "HWB1":
                    values = new[] { "40", "50", "70", "90" };
                    break;
                case "HWB2":
                    values = new[] { "60", "90", "110", "170" };
                    break;
                case "HWB3":
                    values = new[] { "80", "100", "120", "170" };
                    break;
                case "HWB4":
                    values = new[] { "80", "100", "120", "170" };
                    break;
                case "HWB5":
                    values = new[] { "25", "35", "45", "70" };
                    break;
                case "HWB6":
                    values = new[] { "30", "50", "60", "90" };
                    break;
                case "HWB7":
                    values = new[] { "50", "80", "110", "170" };
                    break;
                case "HWB8":
                    values = new[] { "35", "45", "55", "80" };
                    break;
                case "HWB9":
                    values = new[] { "60", "90", "130", "160" };
                    break;
                case "HWB10":
                    values = new[] { "60", "90", "110", "150" };
                    break;
                case "HWB11":
                    values = new[] { "90", "90", "110", "150" };
                    break;
                case "HWB12":
                    values = new[] { "25", "35", "45", "70" };
                    break;
                case "HWB13":
                    values = new[] { "30", "50", "60", "90" };
                    break;
                case "HWB14":
                    values = new[] { "50", "80", "110", "170" };
                    break;
                case "HWB15":
                    values = new[] { "75", "75", "75", "110" };
                    break;
                case "HWB16":
                    values = new[] { "90", "90", "110", "150" };
                    break;
                case "HWB17":
                    values = new[] { "25", "35", "45", "70" };
                    break;
                case "HWB18":
                    values = new[] { "30", "50", "60", "90" };
                    break;
                case "HWB19":
                    values = new[] { "50", "80", "110", "170" };
                    break;
                case "HWB20":
                    values = new[] { "35", "45", "55", "80" };
                    break;
                case "HWB21":
                    values = new[] { "60", "90", "130", "160" };
                    break;
                case "HWB22":
                    values = new[] { "60", "90", "110", "150" };
                    break;
                case "HWB23":
                    values = new[] { "90", "90", "110", "150" };
                    break;
                case "HWB24":
                    values = new[] { "25", "35", "45", "70" };
                    break;
                case "HWB25":
                    values = new[] { "30", "50", "60", "90" };
                    break;
                case "HWB26":
                    values = new[] { "75", "75", "75", "110" };
                    break;
                case "HWB27":
                    values = new[] { "75", "75", "75", "110" };
                    break;
                case "HWB28":
                    values = new[] { "80", "100", "120", "170" };
                    break;

                default:
                    break;
            }

            return values;
        }

        /// <summary>
        /// Return conventional or seismic design HAZUS class according to year built
        /// </summary>
        /// <param name="conventional">Conventional HAZUS Class as string</param>
        /// <param name="seismic_design">Seismic Design HAZUS Class as string</param>
        /// <param name="year">Year built as int</param>
        /// <returns></returns>
        public string CheckSeismicConsideration(string conventional, string seismic_design, int year)
        {
            if (year < 1990)
            {
                return conventional;
            }
            else if (year >= 1990)
            {
                return seismic_design;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Calcuate interpolated skew consideration values according to bridge skew value and HAZUS class
        /// </summary>
        /// <param name="skew">Skew value of bridge in string format</param>
        /// <param name="HAZUSClass">Assigned HAZUS class of bridge</param>
        /// <returns>Interpolated consideration values as array of 4 doubles for applying to slight, moderate, extensive and complete values</returns>
        public double[] HAZUS_Skew_Values(string skew, string HAZUSClass)
        {
            int num = 0;
            int class_num = int.Parse(HAZUSClass.Remove(0, 3));

            // HAZUS skew values contains 5 tables. The table used is decied by HAZUS class.
            switch (class_num)
            {
                case 1:
                    num = 5;
                    break;
                case 2:
                    num = 5;
                    break;
                case 3:
                    num = 5;
                    break;
                case 4:
                    num = 5;
                    break;
                case 5:
                    num = 1;
                    break;
                case 6:
                    num = 1;
                    break;
                case 7:
                    num = 1;
                    break;
                case 8:
                    num = 2;
                    break;
                case 9:
                    num = 2;
                    break;
                case 10:
                    num = 2;
                    break;
                case 11:
                    num = 2;
                    break;
                case 12:
                    num = 3;
                    break;
                case 13:
                    num = 3;
                    break;
                case 14:
                    num = 3;
                    break;
                case 15:
                    num = 4;
                    break;
                case 16:
                    num = 4;
                    break;
                case 17:
                    num = 1;
                    break;
                case 18:
                    num = 1;
                    break;
                case 19:
                    num = 1;
                    break;
                case 20:
                    num = 1;
                    break;
                case 21:
                    num = 1;
                    break;
                case 22:
                    num = 2;
                    break;
                case 23:
                    num = 2;
                    break;
                case 24:
                    num = 3;
                    break;
                case 25:
                    num = 3;
                    break;
                case 26:
                    num = 4;
                    break;
                case 27:
                    num = 4;
                    break;
                case 28:
                    num = 5;
                    break;
                default:
                    break;
            }

            double[][] skewValues = new double[4][];
            switch (num)
            {
                case 1:
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.93, 0.97, 0.94 };
                    skewValues[2] = new double[] { 30.0, 0.81, 0.77, 0.77, 0.73 };
                    skewValues[3] = new double[] { 45.0, 0.74, 0.72, 0.74, 0.72 };
                    break;

                case 2:
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.82, 0.94, 0.84, 0.83 };
                    skewValues[2] = new double[] { 30.0, 0.71, 0.67, 0.69, 0.7 };
                    skewValues[3] = new double[] { 45.0, 0.71, 0.64, 0.68, 0.68 };
                    break;

                case 3:
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 1.0, 0.98, 0.97, 0.95 };
                    skewValues[2] = new double[] { 30.0, 0.94, 0.88, 0.94, 0.87 };
                    skewValues[3] = new double[] { 45.0, 0.94, 0.88, 0.94, 0.87 };
                    break;

                case 4:
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.97, 0.97, 1.0 };
                    skewValues[2] = new double[] { 30.0, 0.89, 0.81, 0.86, 0.9 };
                    skewValues[3] = new double[] { 45.0, 0.89, 0.81, 0.86, 0.9 };
                    break;

                case 5:
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[2] = new double[] { 30.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[3] = new double[] { 45.0, 1.0, 1.0, 1.0, 1.0 };
                    break;

                default:
                    break;
            }
            return Interpolator(skewValues, skew);
        }

        /// <summary>
        /// Calcuate interpolated skew consideration values according to bridge skew value and custom class
        /// </summary>
        /// <param name="skew">Skew value of bridge in string format</param>
        /// <param name="HAZUSClass">Assigned custom class of bridge</param>
        /// <returns>Interpolated consideration values as array of 4 doubles for applying to slight, moderate, extensive and complete values</returns>
        public double[] TexasSpecificSkewValues(string skew, string BridgeClass)
        {
            double[][] skewValues = new double[4][];
            string str = BridgeClass;
            if (str != null)
            {
                if (str == "MCSTEEL")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.97, 0.97, 1.0 };
                    skewValues[2] = new double[] { 30.0, 0.89, 0.81, 0.86, 0.9 };
                    skewValues[3] = new double[] { 45.0, 0.89, 0.81, 0.86, 0.9 };
                }
                else if (str == "MSSTEEL")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 1.0, 0.98, 0.97, 0.95 };
                    skewValues[2] = new double[] { 30.0, 0.94, 0.88, 0.94, 0.87 };
                    skewValues[3] = new double[] { 45.0, 0.94, 0.88, 0.94, 0.87 };
                }
                else if (str == "MSPC")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.93, 0.97, 0.94 };
                    skewValues[2] = new double[] { 30.0, 0.81, 0.77, 0.77, 0.73 };
                    skewValues[3] = new double[] { 45.0, 0.74, 0.72, 0.74, 0.72 };
                }
                else if (str == "MSRC")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.93, 0.97, 0.94 };
                    skewValues[2] = new double[] { 30.0, 0.81, 0.77, 0.77, 0.73 };
                    skewValues[3] = new double[] { 45.0, 0.74, 0.72, 0.74, 0.72 };
                }
                else if (str == "MCRC-Slab")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.82, 0.94, 0.84, 0.83 };
                    skewValues[2] = new double[] { 30.0, 0.71, 0.67, 0.69, 0.7 };
                    skewValues[3] = new double[] { 45.0, 0.71, 0.64, 0.68, 0.68 };
                }
                else if (str == "MSRC-Slab")
                {
                    skewValues[0] = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0 };
                    skewValues[1] = new double[] { 15.0, 0.94, 0.93, 0.97, 0.94 };
                    skewValues[2] = new double[] { 30.0, 0.81, 0.77, 0.77, 0.73 };
                    skewValues[3] = new double[] { 45.0, 0.74, 0.72, 0.74, 0.72 };
                }
            }
            return this.Interpolator(skewValues, skew);
        }

        /// <summary>
        /// Linear interpolation of skew consideration values accoridng to skew consideration values and bridge skew value
        /// </summary>
        /// <param name="skewValues">4*5 double array, 4 rows repesenting: 0, 15, 30, and 45 degree valuse and 5 columns representing no damage, sligth, moderate, extensive and complete states</param>
        /// <param name="skew">Skew value of bridge in string format</param>
        /// <returns>Interpolated consideration values as array of 4 doubles for applying to slight, moderate, extensive and complete values</returns>
        public double[] Interpolator(double[][] skewValues, string skew)
        {
            double[] numArray = new double[4];
            double num = double.Parse(skew);
            if (num >= 45.0)
            {
                numArray = new double[] { skewValues[3][1], skewValues[3][2], skewValues[3][3], skewValues[3][4] };
            }
            else
            {
                int index = -1;
                int num3 = -1;
                if (num < 15.0)
                {
                    index = 0;
                    num3 = 1;
                }
                else if ((num >= 15.0) && (num < 30.0))
                {
                    index = 1;
                    num3 = 2;
                }
                else if ((num >= 30.0) && (num < 45.0))
                {
                    index = 2;
                    num3 = 3;
                }
                int num4 = 1;
                while (true)
                {
                    if (num4 >= 5)
                    {
                        break;
                    }
                    numArray[num4 - 1] = (((skewValues[num3][num4] - skewValues[index][num4]) / (skewValues[num3][0] - skewValues[index][0])) * (num - skewValues[index][0])) + skewValues[index][num4];
                    num4++;
                }
            }
            return numArray;
        }

        /// <summary>
        /// Convert a delimetered text file into C# DataTable format
        /// </summary>
        /// <param name="filePath">Path to text file</param>
        /// <param name="delimeter">Delimeter used in text file</param>
        /// <returns>Converted text file in DataTable format</returns>
        public DataTable ConvertTxtFileToDataTable(string filePath, char delimeter)
        {

            DataTable tbl = new DataTable();

            //Read column names
            string[] colNames = System.IO.File.ReadLines(filePath).First().Split(delimeter);
            foreach (string colName in colNames)
            {
                tbl.Columns.Add(new DataColumn(colName));
            }

            //read all lines but the header
            string[] lines = System.IO.File.ReadAllLines(filePath).Skip(1).ToArray();

            foreach (string line in lines)
            {
                var colData = line.Split(delimeter);

                DataRow dr = tbl.NewRow();

                int Index = 0;
                foreach (var col in colNames)
                {
                    dr[col] = colData[Index];
                    Index++;
                }
                tbl.Rows.Add(dr);

            }
            return tbl;
        }

        /// <summary>
        /// Convert and write DataTable into comma-delimetered text file
        /// </summary>
        /// <param name="dataTable">DataTable to be converted</param>
        /// <param name="path">Path to write the file</param>
        public void WriteDataTableToCSV(DataTable dataTable, string path)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dataTable.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dataTable.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(path, sb.ToString());
        }


        /* Interface Objects Manipulation:
         * 
         * Interface objects including progress bar and rich textboxes are controlled in a different thread
         * than the one doing the analysis, Hence analysis thread cannot change them directly.
         * For occasions that these objects need to be changed by the analysis thread,
         * (Ex: updating the progress bar after analysing each row is completed), following variables and
         * functions are defined to make it possible.
        */

        private delegate void ScrollEnd(RichTextBox control);

        private delegate void SetBarMax(ProgressBar control, int max);

        private delegate void SetBarValue(ProgressBar control, int value);

        private delegate void SetTextOnControl(Control controlToChange, string message);

        private delegate void StepBar(ProgressBar control);


        // Scroll rich texbox to the last line
        public void RichTextboxScrolltoEnd(RichTextBox controlToChange)
        {
            if (!controlToChange.InvokeRequired)
            {
                controlToChange.SelectionStart = controlToChange.Text.Length;
                controlToChange.ScrollToCaret();
            }
            else
            {
                ScrollEnd method = new ScrollEnd(this.RichTextboxScrolltoEnd);
                object[] args = new object[] { controlToChange };
                controlToChange.Invoke(method, args);
            }
        }

        // Add text to rich textbox
        public void RichTextboxSetText(Control controlToChange, string message)
        {
            if (!controlToChange.InvokeRequired)
            {
                controlToChange.Text = controlToChange.Text + message;
            }
            else
            {
                SetTextOnControl method = new SetTextOnControl(this.RichTextboxSetText);
                object[] args = new object[] { controlToChange, message };
                controlToChange.Invoke(method, args);
            }
        }

        // Set max value of progress bar
        public void ProgressBarSetMax(ProgressBar controlToChange, int max)
        {
            if (!controlToChange.InvokeRequired)
            {
                controlToChange.Maximum = max;
            }
            else
            {
                SetBarMax method = new SetBarMax(this.ProgressBarSetMax);
                object[] args = new object[] { controlToChange, max };
                controlToChange.Invoke(method, args);
            }
        }

        // Set value of progress bar
        public void ProgressBarSetValue(ProgressBar controlToChange, int value)
        {
            if (!controlToChange.InvokeRequired)
            {
                controlToChange.Value = value;
            }
            else
            {
                SetBarValue method = new SetBarValue(this.ProgressBarSetValue);
                object[] args = new object[] { controlToChange, value };
                controlToChange.Invoke(method, args);
            }
        }

        // Update progress bar value to +1 step
        public void ProgressBarStep(ProgressBar controlToChange)
        {
            if (!controlToChange.InvokeRequired)
            {
                controlToChange.PerformStep();
            }
            else
            {
                StepBar method = new StepBar(this.ProgressBarStep);
                object[] args = new object[] { controlToChange };
                controlToChange.Invoke(method, args);
            }
        }

    }




}